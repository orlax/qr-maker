#QR MAKER    

this is a qr maker web application working on node with express. 

it has a catch all configuration that serves an html file. This file loads the qrcode.js library and creates a qr code with any path given. 


the important part are the index.html file and the qrcode.js library. it would be possible to port this to any number of webservices that can
provide the catch all links functionality from the express application. 

to run just install express and run the index.js file. 

"""npm install express"""
"""node index.js"""